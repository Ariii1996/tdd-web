import { EMAIL_REGEX, PASS_REGEX } from '../constants/regex';

export const isValidEmail = email => {
  if (!EMAIL_REGEX.test(email)) alert("El mail introducido es incorrecto");
  return EMAIL_REGEX.test(email);
}

export const isValidPassword = (password) => {
  if (!PASS_REGEX.test(password)) alert("La contraseña debe tener 8 o más caracteres");
  return PASS_REGEX.test(password)
};

export const isSamePassword = (password, confirmPassword) => {
  if (password !== confirmPassword) alert("No se introdujo la misma contraseña en ambos campos");
  return password === confirmPassword;
};

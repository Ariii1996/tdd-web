import React, {useState} from 'react';
import {Link, useHistory} from "react-router-dom";

import {isValidEmail, isValidPassword} from '../../../utils/validations';
import Input from '../../components/Input';

import styles from './styles.module.scss';
import GoogleSignIn from "../../components/GoogleSignInButton";
import {ROUTES} from '../../../constants/routes';
import { setValue} from '../../../services/LocalStorageService'
import { post } from '../../../utils/requests'
import firebase from "../../../Firebase";

function Login() {
  const history = useHistory();

  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const handleUserChange = event => setEmail(event?.currentTarget?.value);
  const handlePassChange = event => setPassword(event?.currentTarget?.value);

  const handleSubmit = event => {
      if (isValidEmail(email) && isValidPassword(password)) {
        firebase.auth().signInAnonymously()
        .then(() => {
          post({ body: {email, password}, apiRoute: 'api/users/signIn'})
            .then(res => {
              setValue("user", res.data);
              history.push(ROUTES.CONTENT);
            })
            .catch(err => console.log(err))
          })
        .catch((error) => {
          console.log(error)
        });
      }
      event.preventDefault();
  };
  
  return (
    <div className={styles.container}>
      <form className={`column center ${styles.loginFormContainer}`} onSubmit={handleSubmit}>
        <Input
          name="email"
          label="Email"
          className="m-bottom-4 full-width column"
          inputClassName={styles.loginInput}
          type="email"
          inputType="text"
          onChange={handleUserChange}
          value={email}
          required
        />
        <Input
          name="password"
          label="Contraseña"
          className="m-bottom-4 full-width column"
          inputClassName={styles.loginInput}
          type="password"
          inputType="text"
          value={password}
          onChange={handlePassChange}
          required
        />
        <div className={styles.loginButtonContainer}>
          <button type="submit" className={`row middle center m-bottom-4 ${styles.loginButton}`}>
            <span className={`${styles.loginButtonText} bold text-uppercase`}>
              Ingresar
            </span>
          </button>
        </div>
      </form>
        <div className={styles.registerButtonContainer}>
          <Link className={styles.registerButton} to="/signUp">Registrarse</Link>
        </div>
        <div className={styles.googleButtonContainer}>
          <GoogleSignIn/>
        </div>
      </div>
    );
  }


export default Login;

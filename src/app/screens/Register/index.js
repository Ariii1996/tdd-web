import React, { useState } from 'react';
import { useHistory, useLocation } from "react-router-dom";
import { isValidEmail, isValidPassword, isSamePassword } from '../../../utils/validations';
import Input from '../../components/Input';
import defaultPic from '../../assets/default-user-icon.jpg'
import { getValue, removeValue, setValue } from "../../../services/LocalStorageService";
import { post, patch } from '../../../utils/requests'
import styles from './styles.module.scss';
import { ROUTES } from '../../../constants/routes';
import firebase from "../../../Firebase";
import ReactLoading from 'react-loading';

const DONE = 2;

function Register() {
  const history = useHistory();
  let location = useLocation();

  const user = getValue("user")

  const register_screen = location.pathname === ROUTES.REGISTER

  const [profilePic, setProfilePic] = useState(register_screen ? null : user.profilePic);
  const [name, setName] = useState(register_screen ? '' : user.name);
  const [surname, setSurname] = useState(register_screen ? '' : user.surname);
  const [birthDate, setBirthDate] = useState((register_screen || !user.birthDate) ? '' : user.birthDate.substring(0,10));
  const [email, setEmail] = useState(register_screen ? '' : user.email);
  const [password, setPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [imageInput, setimageInput] = useState(null);
  const [profilePicName, setProfilePicName] = useState('');
  const [profilePicFile, setProfilePicFile] = useState(null);
  const [loading, setLoading] = useState(false);

  const handleUserChange = event => setEmail(event?.currentTarget?.value);
  const handleNameChange = event => setName(event?.currentTarget?.value);
  const handleSurnameChange = event => setSurname(event?.currentTarget?.value);
  const handleBirthDateChange = event => setBirthDate(event?.currentTarget?.value);
  const handlePassChange = event => setPassword(event?.currentTarget?.value);
  const handleNewPassChange = event => setNewPassword(event?.currentTarget?.value);
  const handleConfirmPassChange = event => setConfirmPassword(event?.currentTarget?.value);

  const handleSubmit = event => {
    if (register_screen){
      if (isValidEmail(email) && isValidPassword(password) && isSamePassword(password, confirmPassword)){
        firebase.auth().signInAnonymously()
        .then(() => {
          const newUser = {
            name: name,
            surname: surname,
            birthDate: birthDate,
            email: email,
            password: password,
            provider: 'email'
          }
          if (!!profilePicFile) {
            setLoading(true)
            var uploadTask = firebase.storage().ref().child(profilePicName).put(profilePicFile);

            // Register three observers:
            // 1. 'state_changed' observer, called any time the state changes
            // 2. Error observer, called on failure
            // 3. Completion observer, called on successful completion
            uploadTask.on('state_changed', function(snapshot){
              // Observe state change events such as progress, pause, and resume
              // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
              var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
              console.log('Upload is ' + progress + '% done');
              switch (snapshot.state) {
                case firebase.storage.TaskState.PAUSED: // or 'paused'
                  console.log('Upload is paused');
                  break;
                case firebase.storage.TaskState.RUNNING: // or 'running'
                  console.log('Upload is running');
                  break;
              }
            }, function(error) {
              console.log("fallo subiendo foto de perfil", error)
              setLoading(false)
            }, function() {
              // Handle successful uploads on complete
              uploadTask.snapshot.ref.getDownloadURL().then(function(url) {
                newUser.profilePic = url
                post({ body: newUser, apiRoute: 'api/users/signUp'})
                .then(res => {
                  setValue("user", res.data);
                  history.replace(ROUTES.CONTENT);
                })
                .catch(err => {
                  setLoading(false)
                  console.log(err)
                })
              });
            });
          }else {
            newUser.profilePic = defaultPic
            post({ body: newUser, apiRoute: 'api/users/signUp'})
                .then(res => {
                  setValue("user", res.data);
                  history.replace(ROUTES.CONTENT);
                })
                .catch(err => {
                  console.log(err)
                }
                  )
          }
        })
        .catch((error) => {
          console.log(error)
        });
      }
    } 
    else{
      var updatedUser
      if (user.provider === "email"){
        if (!isValidEmail(email) || !isValidPassword(password)) return
        if (newPassword.length !== 0){
          if (!isValidPassword(newPassword) || !isSamePassword(newPassword, confirmPassword)) return
        }
        updatedUser = {
          name: name,
          surname: surname,
          birthDate: birthDate,
          email: email,
          password: password,
          newPassword: newPassword.length !== 0 ? newPassword : undefined 
        }
      }else{
        updatedUser = {
          name: name,
          email: email,
          surname: surname,
        }
      }
      if (!!profilePicFile) {
        setLoading(true)
        var uploadTask = firebase.storage().ref().child(profilePicName).put(profilePicFile);

        // Register three observers:
        // 1. 'state_changed' observer, called any time the state changes
        // 2. Error observer, called on failure
        // 3. Completion observer, called on successful completion
        uploadTask.on('state_changed', function(snapshot){
          // Observe state change events such as progress, pause, and resume
          // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
          var progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
          console.log('Upload is ' + progress + '% done');
          switch (snapshot.state) {
            case firebase.storage.TaskState.PAUSED: // or 'paused'
              console.log('Upload is paused');
              break;
            case firebase.storage.TaskState.RUNNING: // or 'running'
              console.log('Upload is running');
              break;
          }
        }, function(error) {
          console.log("fallo subiendo foto de perfil", error)
          setLoading(false)
        }, function() {
          // Handle successful uploads on complete
          uploadTask.snapshot.ref.getDownloadURL().then(function(url) {
            updatedUser.profilePic = url
            patch({ body: updatedUser, apiRoute: 'api/users/me', headers: {'x-auth-token': user.accessToken}})
            .then(res => {
              setValue("user", res.data);
              history.replace(ROUTES.CONTENT);
              })
            .catch(err => {
              setLoading(false)
              console.log(err)
            })
          });
        });
      }else {
        updatedUser.profilePic = profilePic
        patch({ body: updatedUser, apiRoute: 'api/users/me', headers: {'x-auth-token': user.accessToken}})
        .then(res => {
          setValue("user", res.data);
          history.replace(ROUTES.CONTENT);
        })
        .catch(err => console.log(err))
      }
    }
    event.preventDefault();
  };

  const handleImagePicker = event => {
    const reader = new FileReader();
    reader.fileName = event.target.files[0].name
    reader.onload = (readerEvt) => {
      if(reader.readyState === DONE){
        setProfilePic(reader.result);
        setProfilePicName(readerEvt.target.fileName)
      }
    }   
    
    if (!!event.target.files[0] && event.target.files[0].type.match('image.*')){
      reader.readAsDataURL(event.target.files[0]);
      setProfilePicFile(event.target.files[0])
    }

  }

  return (
    loading ? <ReactLoading className={styles.loader} type={"spin"} color={"grey"} height={200} width={200}/> :
    <div className={styles.container}>
      <img className={styles.profilePic} src={profilePic || defaultPic} alt="zerf-icon" />
      <input 
        style={{display: 'none'}}
        type="file"
        accept="image/*"
        name="profilePic"
        onChange={handleImagePicker} 
        ref={imageInput => setimageInput(imageInput)}
      />
      <button className={`row middle center m-bottom-4 ${styles.profilePicButton}`} onClick={() => imageInput.click()}>
        <span className={`${styles.profilePicButtonText} bold text-uppercase`}>
          {register_screen ? "Agregar foto" : "Cambiar foto"}
        </span>
      </button>  
      <form className={`column center ${styles.registerFormContainer}`} onSubmit={handleSubmit}>
        <Input
          name="name"
          label="Nombre"
          className="m-bottom-4 full-width column"
          inputType="text"
          value={name}
          onChange={handleNameChange}
          required
        />
        <Input
          name="surname"
          label="Apellido"
          className="m-bottom-4 full-width column"
          inputType="text"
          value={surname}
          onChange={handleSurnameChange}
          required
        />
        {register_screen && 
          <Input
            name="email"
            label="Email"
            className="m-bottom-4 full-width column"
            type="email"
            inputType="text"
            onChange={handleUserChange}
            value={email}
            required
          />
        }
        {(register_screen || (!register_screen && user.provider !== "google")) &&  
        <>
          <Input
            name="birthDate"
            label="Fecha de nacimiento"
            className="m-bottom-4 full-width column"
            type="date" 
            data-date-split-input="true" 
            value={birthDate}
            onChange={handleBirthDateChange}
            required
          />
          <Input
            name="password"
            label={register_screen ? "Contraseña" : "Contraseña actual"}
            className="m-bottom-4 full-width column"
            type="password"
            inputType="text"
            value={password}
            onChange={handlePassChange}
            required
          />
          {!register_screen && 
            <Input
              name="newPassword"
              label="Nueva constraseña"
              className="m-bottom-4 full-width column"
              type="password"
              inputType="text"
              value={newPassword}
              onChange={handleNewPassChange}
            />
          }
          <Input
            name="passwordConfirm"
            label="Repetir contraseña"
            className="m-bottom-4 full-width column"
            type="password"
            inputType="text"
            value={confirmPassword}
            onChange={handleConfirmPassChange}
            required={newPassword.length !== 0}
          />
        </>
        }
        <div className={styles.registerButtonContainer}>
          <button loading={loading} type="submit" className={`row middle center m-bottom-4 ${styles.registerButton}`}>
            <span className={`${styles.registerButtonText} bold text-uppercase`}>
              {register_screen ? "Registrarse" : "Confirmar cambios"}
            </span>
          </button>  
        </div>
      </form>
    </div>
  );
}

export default Register;

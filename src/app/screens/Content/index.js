import React, {useEffect, useState} from "react";
import {useHistory} from "react-router-dom";
import {ROUTES} from '../../../constants/routes';
import {Paper} from "@material-ui/core";
import {deleteMethod, get, post} from "../../../utils/requests";
import {getValue} from "../../../services/LocalStorageService"
import ReactLoading from 'react-loading';
import styles from './styles.module.scss';
import FilePreview from "../../components/FilePreview";
import RecursiveTreeView from "../../components/TreeView";
import FileOwnership from "../../components/FileOwnership";
import {ContextMenu, ContextMenuTrigger, MenuItem} from 'react-contextmenu'
import './react-contextmenu.css'
import './custom.css'
import SetPermissions from "../../components/SetPermissions";
import * as PropTypes from "prop-types";
import {Header} from "../../components/Header";
import {FileContent} from "../../components/FileContent";
import {DirContent} from "../../components/DirContent";
import {contentMapper} from "./helpers";
import {NewFolderDialog} from "../../components/NewFolderDialog";


Header.propTypes = {
  onRefresh: PropTypes.func,
  path: PropTypes.string,
  onTreeIconClick: PropTypes.func,
  onInfoIconClick: PropTypes.func
};

FileContent.propTypes = {
  contentCardContextMenuID: PropTypes.string,
  localState: PropTypes.any,
  onClick: PropTypes.func,
  extension: PropTypes.any,
  onDeleteClick: PropTypes.func,
  attributes: PropTypes.shape({
    selectedClassName: PropTypes.string,
    className: PropTypes.string,
    dividerClassName: PropTypes.string,
    disabledClassName: PropTypes.string
  })
};

DirContent.propTypes = {
  dirContextMenuID: PropTypes.string,
  localState: PropTypes.any,
  onDoubleClick: PropTypes.func,
  onClick: PropTypes.func,
  attributes: PropTypes.shape({
    selectedClassName: PropTypes.string,
    className: PropTypes.string,
    dividerClassName: PropTypes.string,
    disabledClassName: PropTypes.string
  })
};

NewFolderDialog.propTypes = {
  open: PropTypes.bool,
  onClose: PropTypes.func,
  value: PropTypes.string,
  onChange: PropTypes.func,
  onClick: PropTypes.func
};

function DirectoryCreationContextMenu(props) {
  return <ContextMenu id={props.id}>
    <MenuItem
        onClick={props.onClick}
        attributes={props.attributes}
    >
      Crear Carpeta
    </MenuItem>
  </ContextMenu>;
}

DirectoryCreationContextMenu.propTypes = {
  id: PropTypes.string,
  onClick: PropTypes.func,
  attributes: PropTypes.shape({
    selectedClassName: PropTypes.string,
    className: PropTypes.string,
    dividerClassName: PropTypes.string,
    disabledClassName: PropTypes.string
  })
};

function DirectoryDeletionContextMenu(props) {
  return <ContextMenu id={props.id}>
    <MenuItem
        onClick={props.onClick}
        attributes={props.attributes}
    >
      Eliminar carpeta.
    </MenuItem>
  </ContextMenu>;
}

DirectoryDeletionContextMenu.propTypes = {
  id: PropTypes.string,
  onClick: PropTypes.func,
  attributes: PropTypes.shape({
    selectedClassName: PropTypes.string,
    className: PropTypes.string,
    dividerClassName: PropTypes.string,
    disabledClassName: PropTypes.string
  })
};
export default function Content() {
  const history = useHistory();

  const [refresh, setRefresh] = useState(false)
  const [content, setContent] = useState([]);
  const [loading, setLoading] = useState(true);
  const [previewURL, setPreviewURL] = useState("")
  const [currentFileName, setCurrentFileName] = useState("")
  const [currentFileId, setCurrentFileId] = useState("")
  const [path, setPath] = useState("/")
  const [showTree, setShowTree] = useState(false)
  const [showInfo, setShowInfo] = useState(false)
  const [openDialog, setOpenDialog] = useState(false);
  const [newDirectoryName, setNewDirectoryName] = useState('')
  const [newPermissionRole, setNewPermissionRole] = useState("Viewer")
  const [newPermissionEmail, setNewPermissionEmail] = useState("")
  const [canEditCurrentFile, setCanEditCurrentFile] = useState(false)
  const [tree, setTree] = useState([])
  const [sharedTree, setSharedTree] = useState([])
  const [sharedUsers, setSharedUsers] = useState([])
  const [refreshSharedUsers, setRefreshSharedUsers] = useState(false)

  const onRefresh = () => setRefresh(!refresh)

  useEffect(() => {
    var user = getValue("user");
    if (!user) {
      setRefresh(!refresh)
      return
    } else setLoading(false);
    if (!user) history.replace(ROUTES.LOGIN)
    else {
      get({
        apiRoute: 'api/files/',
        headers: {'x-auth-token': user.accessToken},
        params: {path: path}
      })
          .then(function (response) {
            setContent(response.data.filesFound)
          })
          .catch((error) => {
            console.log("Un error has occurred: ", error);
          })

      get({apiRoute: 'api/files/tree/', headers: {'x-auth-token': user.accessToken}})
          .then(function (response) {
            if(response.data.length){
              if(response.data[0].id === "root") {
                setTree(response.data[0])
                if(response.data[1]) setSharedTree(response.data[1])
              } else {
                setTree(
                  {
                    children:[],
                    name: "/",
                    path: "/",
                    id: "root"
                  }
                )
                setSharedTree(response.data[0])
              }
            }
          })
          .catch((error) => {
            console.log("ERROR EN EL GET DEL TREE: ", error);
          })
    }
    setShowInfo(false)
    setPreviewURL("")
    setCurrentFileName("")
    setCurrentFileId("")
    setCanEditCurrentFile(false)
  }, [refresh, path]);

  function handleTreeClick(newVal) {
    setPath(newVal)
  }

  const getExtension = file => {
    const aux = file.split(".")
    return aux[aux.length - 1]
  }

  const ID = 'ID'
  const contentCardContextMenuID = "contentCardContextMenuID"
  const DirContextMenuID = "DirContextMenuID"

  const attributes = {
    className: 'custom-root',
    disabledClassName: 'custom-disabled',
    dividerClassName: 'custom-divider',
    selectedClassName: 'custom-selected'
  }

  const handleCreateDirectory = () => {
    setOpenDialog(true);
  }

  const handleFileDeletion = (fileName) => {
    alert("Deleting file: " + fileName)
    //TODO ver msg body
    const file = {
      path: path + fileName,
    }
    deleteMethod({
          params: file,
          //TODO ver endpoint
          apiRoute: 'api/files/file/',
          headers: {'x-auth-token': getValue("user").accessToken}
        }
    )
        .then(function (response) {
          onRefresh()
        })
        .catch(function (error) {
          console.log("ERROR BORRANDO FILE: ", error);
        })
  }

  const handleDirDeletion = (dirName) => {
    alert("Deleting file: " + dirName)
    //TODO ver msg body
    const file = {
      path: path + dirName
    }
    deleteMethod({
          params: file,
          //TODO ver endpoint
          apiRoute: 'api/files/directory/',
          headers: {'x-auth-token': getValue("user").accessToken}
        }
    )
        .then(function (response) {
          onRefresh()
        })
        .catch(function (error) {
          console.log("ERROR BORRANDO DIR: ", error);
        })
  }

  const handleClose = () => {
    setNewDirectoryName('')
    setOpenDialog(false);
  };

  const handleAcceptDirectoryCreation = () => {
    if (!!newDirectoryName) {
      const file = {
        type: 'directory',
        name: newDirectoryName,
        path: path
      }
      post({body: file, apiRoute: 'api/files/', headers: {'x-auth-token': getValue("user").accessToken}})
          .then(function (response) {
            onRefresh()
          })
          .catch(function (error) {
            console.log("ERROR CREANDO CARPETA: ", error);
          })
    }
    setNewDirectoryName('')
    setOpenDialog(false);
  }

  const handleDirectoryNameChange = event => setNewDirectoryName(event?.currentTarget?.value);

  const handleOpenDirectory = directoryName => {
    setPath(path + directoryName + '/')
  }

  const handlePermissionSubmit = event => {
    event.preventDefault()
    
    const user = getValue("user");

    const body = {
      hostEmail: user.email,
      guestEmail: newPermissionEmail,
      fileName: currentFileName,
      fileId: currentFileId,
      canShare: newPermissionRole === "Editor"
    }

    post({body, apiRoute: '/api/users/fileShareRequest', headers: {'x-auth-token': user.accessToken}})
      .then(function (response) {
        onRefresh()
      })
      .catch(function (error) {
        console.log("ERROR COMPARTIENDO ARCHIVO: ", error);
      })
  }

  const getCanEdit = () => canEditCurrentFile;
  const getSharedUsers = () => sharedUsers;

  return (
      loading ? <ReactLoading className={styles.loader} type={"spin"} color={"grey"} height={200} width={200}/> :
          <div>
            <Header
                onRefresh={() => onRefresh()}
                path={path}
                onTreeIconClick={() => setShowTree((prevState) => !prevState)}
                onInfoIconClick={() => setShowInfo((prevState) => !prevState)}
            />

            <ContextMenuTrigger id={ID}>
              <div className={styles.mainContentDiv}>
                <div className={showTree ? styles.treeView : styles.hidden}>
                  <RecursiveTreeView
                      onClickHandler={handleTreeClick}
                      myUnitTree={tree}
                      sharedTree={sharedTree}
                  />
                </div>
                <div className={styles.cardsContainer}>
                  {contentMapper(content, contentCardContextMenuID, setPreviewURL, setCurrentFileName, setRefreshSharedUsers, refreshSharedUsers,
                      setCanEditCurrentFile, getExtension, handleFileDeletion,setCurrentFileId, setSharedUsers,
                      attributes, DirContextMenuID, handleOpenDirectory, handleDirDeletion, path)}
                </div>
                <div className={showInfo ? styles.filePreview : styles.hidden}>
                  <div title={"File Preview"}>
                    <Paper
                        elevation={24}
                        className={styles.paper}
                    >
                      <FilePreview url={previewURL}/>
                    </Paper>
                  </div>
                  <div>
                    <SetPermissions
                        value={newPermissionRole}
                        setPermission={setNewPermissionRole}
                        emailValue={newPermissionEmail}
                        setEmail={setNewPermissionEmail}
                        handleSubmit={handlePermissionSubmit}
                        getCanEdit={getCanEdit}
                        className={styles.setPermissions}
                    />
                  </div>
                  {path !== "/shared/" && 
                    <div className={styles.fileOwnership}>
                      <FileOwnership getSharedUsers={getSharedUsers} refresh={refreshSharedUsers}/>
                    </div>
                  }         
                </div>
              </div>
            </ContextMenuTrigger>
            {path !== "/shared/" && 
              <>
                <DirectoryCreationContextMenu id={ID} onClick={handleCreateDirectory} attributes={attributes}/>
                <DirectoryDeletionContextMenu id={DirContextMenuID} onClick={handleDirDeletion} attributes={attributes}/>
                <NewFolderDialog open={openDialog} onClose={handleClose} value={newDirectoryName}
                                onChange={handleDirectoryNameChange} onClick={handleAcceptDirectoryCreation}/>
              </>
            }
            {/*<footer>
              <div className={styles.iconsCreditsDiv}>
                Icons made by
                <a href="https://www.flaticon.com/authors/smashicons" title="Smashicons"> Smashicons </a>
                from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a>
              </div>
            </footer>*/}
          </div>
  )
}
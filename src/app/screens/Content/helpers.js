import {FileContent} from "../../components/FileContent";
import {DirContent} from "../../components/DirContent";
import { get } from "../../../utils/requests";
import {getValue} from "../../../services/LocalStorageService"
import React from "react";

export function contentMapper(content, contentCardContextMenuID, setPreviewURL, setCurrentFileName, setRefreshSharedUsers, refreshSharedUsers,
                              setCanEditCurrentFile, getExtension, handleFileDeletion, setCurrentFileId, setSharedUsers,
                              attributes, DirContextMenuID, handleOpenDirectory, handleDirDeletion, path) {

  const handleClick = localState => {
    setCurrentFileName(localState.name)
    setCurrentFileId(localState._id)
    setPreviewURL(localState.url)
    setCanEditCurrentFile(localState.canShare)
    
    get({
      apiRoute: 'api/files/file',
      headers: {'x-auth-token': getValue("user").accessToken},
      params: {path: path + localState.name}
    })
    .then(function (response) {
      setSharedUsers(response.data.sharedWith)
      setRefreshSharedUsers(!refreshSharedUsers)
    })
    .catch((error) => {
      console.log("Un error has occurred: ", error);
    })
  };

  return <>
    {content.map((localState, index) => (
        localState.type === "file" ?
            <FileContent
                key={localState.id}
                contentCardContextMenuID={contentCardContextMenuID}
                localState={localState}
                onClick={() => handleClick(localState)}
                extension={getExtension(localState.name)}
                onDeleteClick={() => handleFileDeletion(localState.name)}
                attributes={attributes}
                path={path}
                />
            :
            <DirContent
                dirContextMenuID={DirContextMenuID}
                localState={localState}
                onDoubleClick={() => handleOpenDirectory(localState.name)}
                onClick={() => handleDirDeletion(localState.name)}
                attributes={attributes}/>
    ))}
  </>;
}
import React, {useEffect, useState} from 'react'
import styles from "./styles.module.scss";
import {Paper} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import {useHistory} from "react-router-dom";
import {ROUTES} from "../../../constants/routes";

function EvaledCode(props) {
  try {
    return (
        <div>
          {eval(props.code)}
        </div>
    )
  } catch (e) {
    alert(e)
    return null
  }
}

function Eval() {
  const [code, setCode] = useState("")

  function handleFileLoad(event) {
    const file = event.target.files[event.target.files.length - 1]
    file.text().then(text => {
      console.log("Text: ", text)
      setCode(text)
    });
    event.target.value = ''
  }

  const history = useHistory();

  return (
      <Paper className={styles.container} elevation={24}>
        <div className={styles.header}>
          <Button
              variant="contained"
              onClick={() => {
                history.push(ROUTES.CONTENT)
                //TODO meter ROUTER navegacion a Content.
                console.log("do sth")
              }}>
            To Home page
          </Button>
          <Button variant="contained" color="primary" type="file">
            <label>
              <input
                  type="file"
                  style={{"display": "none"}}
                  accept={"text/javascript"}
                  onChange={handleFileLoad}
              />
              Upload your script here
            </label>
          </Button>
        </div>
        <div id="evaldiv">
          <Paper
              elevation={24}
              square={false}
              className={styles.paper}
          >
            <EvaledCode code={code}/>
          </ Paper>
        </div>
      </Paper>
  )
}

export default Eval;
import React, {Component} from "react";
import FileUploader from "react-firebase-file-uploader";
import firebase from "../../../Firebase"
import PublishIcon from '@material-ui/icons/Publish';
import styles from './styles.module.scss';
import 'fontsource-roboto';
import CircularProgressWithLabel from "./CircularProgressWithLaberl";
import { post } from "../../../utils/requests";
import { getValue } from "../../../services/LocalStorageService"

class Upload extends Component {
  state = {
    isUploading: false,
    progress: 0,
    fileURL: ""
  };

  handleUploadStart = () => this.setState({isUploading: true, progress: 0});
  handleProgress = progress => this.setState({progress});
  handleUploadError = error => {
    this.setState({isUploading: false});
    console.error(error);
  };
  handleUploadSuccess = filename => {
    this.setState({avatar: filename, progress: 100, isUploading: false});
    const onRefresh = this.props.onRefresh
    const path = this.props.path
    firebase
        .storage()
        .ref()
        .child(filename)
        .getDownloadURL()
        .then(url => {
              this.setState({fileURL: url})
              const file = {
                type: 'file',
                name: filename,
                url: url,
                path: path
              }
              post({ body: file, apiRoute: 'api/files/', headers: {'x-auth-token': getValue("user").accessToken}})
                  .then(function (response) {
                    console.log("Response: ", response);
                    onRefresh()
                  })
                  .catch(function (error) {
                    //Eliminarlo de firebase+
                    console.log("ERROR: ", error);
                  })
            }
        );
  };

  render() {
    return (
      <div>
        <label style={{
          backgroundColor: 'steelblue',
          color: 'white',
          padding: 10,
          borderRadius: 4,
          cursor: 'pointer'
        }}>
          {this.state.isUploading && <span className={styles.circularProgress}><CircularProgressWithLabel
              value={this.state.progress}/></span>}
          <span className={styles.text}>Upload new file</span>
          <PublishIcon className={styles.materialIcons}/>
          <FileUploader
              hidden
              accept=""
              name="avatar"
              storageRef={firebase.storage().ref("")}
              onUploadStart={this.handleUploadStart}
              onUploadError={this.handleUploadError}
              onUploadSuccess={this.handleUploadSuccess}
              onProgress={this.handleProgress}
          />
        </label>
      </div>
    );
  }
}

export default Upload;
import React, {useEffect, useState} from "react"
import {Avatar, Paper} from "@material-ui/core";
import styles from './styles.module.scss';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import {someList} from "./sampleData";
import {useStyles, avatarStyles} from "./materialStyles";
import {getValue} from "../../../services/LocalStorageService"


function FileOwnership({ getSharedUsers, refresh }) {
  const listClasses = useStyles();
  const avatarClasses = avatarStyles();
  const [roles, setRoles] = useState([]);

  useEffect(() => {
    const user = getValue("user");

    const aux = getSharedUsers().map(user => ({
      id: user._id, 
      name: user.name + " " + user.surname, 
      role: user.canShare ? "Editor" : "Read Only"
    }))

    const owner = {
      id: "1", 
      name: user.name + " " + user.surname, 
      role: "Owner"
    }

    const users = aux.length ? [owner].concat(aux) : [owner];
    
    setRoles(users);
  }, [refresh])

  return (
      <div className={styles.main}>
        <Paper
          elevation={24}
          square={false}
          className={styles.paper}
        >
          <List component="nav" className={listClasses.root} aria-label="mailbox folders">
            {roles.map((element, index) => (
              <div id={element.id}>
                <ListItem>
                  <Avatar className={avatarClasses.purple}>{element.name[0]}</Avatar>
                  <Divider orientation="vertical" flexItem style={{"margin": "0px 10px 0px 10px"}}/>
                  <ListItemText
                    primary={element.name}
                    secondary={element.role}
                  />
                </ListItem>
                <Divider/>
              </div>
              )
            )}
          </List>
        </Paper>
      </div>
  )
}

export default FileOwnership
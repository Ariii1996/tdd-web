import React from 'react';
import cx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import IconButton from '@material-ui/core/IconButton';
import FavoriteBorderRounded from '@material-ui/icons/FavoriteBorderRounded';
import Share from '@material-ui/icons/Share';
import { useSoftRiseShadowStyles } from '@mui-treasury/styles/shadow/softRise';
import { useSlopeCardMediaStyles } from '@mui-treasury/styles/cardMedia/slope';
import { useN01TextInfoContentStyles } from '@mui-treasury/styles/textInfoContent/n01';
import TextInfoContent from '@mui-treasury/components/content/textInfo';

const styles = {
  card: {
    maxWidth: 150,
  },
  media: {
    height: "0.03em",
    paddingTop: '56.25%', // 16:9
  },
};
const useStyles = makeStyles(() => ({
  root: {
    maxWidth: 200,
    margin: 'auto',
    marginTop:20,
    width: 200,
  },
  content: {
    justifyContent: 'center'
  },
  avatar: {
    width: 50,
    height: 50,
    border: '2px solid #fff',
    margin: '-48px 32px 0 auto',
    '& > img': {
      margin: 0,
    },
  },
}));

export const ContentCard = React.memo(function PostCard(props) {
  const cardStyles = useStyles();
  const mediaStyles = useSlopeCardMediaStyles();
  const shadowStyles = useSoftRiseShadowStyles();
  const textCardContentStyles = useN01TextInfoContentStyles();
  function handleClick() {
    console.log(props.url);
  }
  return (
    <Card className={cx(cardStyles.root, shadowStyles.root)}
                    onClick={handleClick}>
      <CardMedia
        classes={mediaStyles}
          style={styles.media}
        image={
          process.env.PUBLIC_URL + "/svg/" + props.file_extension + ".svg"
        }
      />
      {/* We don't want an avatar in this card */}
      {/*<Avatar className={cardStyles.avatar} src={'https://i.pravatar.cc/300'} />*/}
      <CardContent className={cardStyles.content}>
        <TextInfoContent
          classes={textCardContentStyles}
          heading={props.file_name}
          // body={
          //   'A nice file description'
          // }
        />
      </CardContent>
      <Box px={2} pb={2} mt={-1}>
        <IconButton>
          <Share />
        </IconButton>
        <IconButton>
          <FavoriteBorderRounded />
        </IconButton>
      </Box>
    </Card>
  );
});

export default ContentCard;
import MenuAppBar from "../NavBar";
import styles from "../../screens/Content/styles.module.scss";
import AccountTreeOutlinedIcon from "@material-ui/icons/AccountTreeOutlined";
import InfoOutlinedIcon from "@material-ui/icons/InfoOutlined";
import React from "react";

export function Header(props) {
  return <>
    <MenuAppBar onRefresh={props.onRefresh} path={props.path}/>
    <div className={styles.infoBar}>
      <AccountTreeOutlinedIcon
          className={styles.treeIcon}
          fontSize={"large"}
          onClick={props.onTreeIconClick}
      />
      <span className={styles.title}>{props.path === "/shared/" ? "Compartidos conmigo" : "Mi unidad"}</span>
      <InfoOutlinedIcon
          className={styles.infoIcon}
          fontSize={"large"}
          onClick={props.onInfoIconClick}
      />
    </div>
  </>;
}
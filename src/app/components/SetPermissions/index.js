import React from "react"
import styles from "./styles.module.scss"
import {Paper} from "@material-ui/core";

function SetPermissions(props) {

  const getValue = () =>{
    return !props.getCanEdit()
  }

  return (
      <div>
        <Paper className={styles.paper} elevation={24}>
          <h2>Manage permissions</h2>
          <form
              className={styles.form}
              onSubmit={props.handleSubmit}
          >
            <input
                className={styles.input}
                type={"email"}
                id={"email"}
                autoFocus={true}
                placeholder={"Email"}
                disabled={getValue()}
                value={props.emailValue}
                onChange={event => props.setEmail(event.target.value)}
            />
            <select
                className={styles.select}
                value={props.value}
                onChange={(event => props.setPermission(event.target.value))}
                disabled={getValue()}
            >
              <option>Viewer</option>
              <option>Editor</option>
            </select>
            <input
                className={styles.submit}
                type="submit"
                value="Submit"
                disabled={getValue()}
            />
          </form>
        </Paper>
      </div>
  );
}

export default SetPermissions
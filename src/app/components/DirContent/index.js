import {ContextMenu, ContextMenuTrigger, MenuItem} from "react-contextmenu";
import styles from "../../screens/Content/styles.module.scss";
import folder from "../../assets/folder.png";
import React from "react";

export function DirContent(props) {
  return <div>
    <ContextMenuTrigger id={props.dirContextMenuID + props.localState.name}>
      <div className={styles.folderBox}>
        <div className={styles.folderContainer}
             onDoubleClick={props.onDoubleClick}>
          <img className={styles.folder} src={folder} alt="zerf-icon"/>
          <p className={styles.folderName}>{props.localState.name}</p>
        </div>
      </div>
    </ContextMenuTrigger>
    <ContextMenu id={props.dirContextMenuID + props.localState.name}>
      <MenuItem
          onClick={props.onClick}
          attributes={props.attributes}
      >
        Eliminar carpeta.
      </MenuItem>
    </ContextMenu>
  </div>;
}
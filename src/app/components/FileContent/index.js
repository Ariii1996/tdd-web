import {ContextMenu, ContextMenuTrigger, MenuItem} from "react-contextmenu";
import styles from "../../screens/Content/styles.module.scss";
import ContentCard from "../ContentCard";
import React from "react";

export function FileContent(props) {
  return <div>
    <ContextMenuTrigger id={props.contentCardContextMenuID + props.localState.name}>
      <div
          className={styles.card}
          onClick={props.onClick}
      >
        <ContentCard
            className={styles.contentCard}

            file_extension={props.extension}
            file_name={props.localState.name}
        />
      </div>
    </ContextMenuTrigger>
    {props.path !== "/shared/" &&
      <ContextMenu id={props.contentCardContextMenuID + props.localState.name}>
        <MenuItem
            onClick={props.onDeleteClick}
            attributes={props.attributes}
        >
          Eliminar archivo.
        </MenuItem>
      </ContextMenu>
    }
  </div>;
}
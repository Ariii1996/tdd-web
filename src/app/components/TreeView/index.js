import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import TreeView from '@material-ui/lab/TreeView';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import TreeItem from '@material-ui/lab/TreeItem';

const useStyles = makeStyles({
  root: {
    height: 110,
    flexGrow: 1,
    maxWidth: 400,
  },
});

export default function RecursiveTreeView(props) {
  const classes = useStyles();

  function onLabelClick(event, path) {
    event.preventDefault()
    props.onClickHandler(path)
  }
  const renderTree = (myUnitTree, sharedTree) => (
    <>
      {myUnitTree &&
        <TreeItem
            key={myUnitTree.id}
            nodeId={myUnitTree.id}
            label={myUnitTree.name}
            onLabelClick={event => onLabelClick(event, myUnitTree.path)}>
          {Array.isArray(myUnitTree.children) ? myUnitTree.children.map((node) => renderTree(node)) : null}
        </TreeItem>
      }
      {sharedTree && 
        <TreeItem
          key={sharedTree.id}
          nodeId={sharedTree.id}
          label={sharedTree.name}
          onLabelClick={event => onLabelClick(event, sharedTree.path)}>
            {Array.isArray(sharedTree.children) ? sharedTree.children.map((node) => renderTree(node)) : null}
        </TreeItem>
      }
    </>
  );

  return (
      <TreeView
          className={classes.root}
          defaultCollapseIcon={<ExpandMoreIcon/>}
          defaultExpanded={['root']}
          defaultExpandIcon={<ChevronRightIcon/>}
      >
        {renderTree(props.myUnitTree, props.sharedTree)}
      </TreeView>
  );
}

import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles({
  root: {
    maxWidth: 345,
    margin: "auto",
  },
});

function ImgMediaCard() {
  const classes = useStyles();

  return (
      <Card className={classes.root}>
        <CardActionArea>
          <CardMedia
              component="img"
              alt="Contemplative Reptile"
              height="140"
              image="https://upload.wikimedia.org/wikipedia/commons/thumb/2/25/Bangkok_Reptiles_Blue_crested_Lizard.jpg/1200px-Bangkok_Reptiles_Blue_crested_Lizard.jpg"
              title="Contemplative Reptile"
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="h2">
              No Preview available
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              Only images, pdfs and audios can be previewed.
              If you think this is an error, please contact Support.
            </Typography>
          </CardContent>
        </CardActionArea>
      </Card>
  );
}


function NoPreview() {
  return (
      <div>
        <ImgMediaCard/>
      </div>
  )
}

export default NoPreview;
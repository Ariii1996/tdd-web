import React from 'react'
import styles from './styles.module.scss'
import NoPreview from "./noPreview"

export default function FilePreview(props) {
  return (
      <div className={styles.container}>
        <div className={styles.filePreview}>
          {
            props.url
                ? <iframe title={props.url} style={{"height": "100%", "width": "100%"}} src={props.url}/>
                : <NoPreview/>
          }
        </div>
      </div>
  )
}
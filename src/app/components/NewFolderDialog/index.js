import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import TextField from "@material-ui/core/TextField";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import React from "react";

export function NewFolderDialog(props) {
  return <Dialog open={props.open} onClose={props.onClose} aria-labelledby="form-dialog-title">
    <DialogTitle id="form-dialog-title">Crear una nueva carpeta</DialogTitle>
    <DialogContent>
      <DialogContentText>
        Ingrese el nombre de su nueva carpeta.
      </DialogContentText>
      <TextField
          autoFocus
          margin="dense"
          id="directoty"
          fullWidth
          value={props.value}
          onChange={props.onChange}
      />
    </DialogContent>
    <DialogActions>
      <Button onClick={props.onClose} color="primary">
        Cancelar
      </Button>
      <Button onClick={props.onClick} color="primary">
        Crear
      </Button>
    </DialogActions>
  </Dialog>;
}